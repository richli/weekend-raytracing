# Ray Tracing in One Weekend in Rust

This is my implementation in Rust of the [Ray Tracing in One
Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) course.

## Divergence from book

This follows the C++ version pretty closely, but I've made a few Rustic changes,
such as using `Option<T>` values, adding `match` statements, turning `for` loops
into iterators, and using traits instead of classes.

Instead of making `Color` and `Point3` type aliases to `Vec3` I make them
distinct (implementing the "newtype pattern").

I've also chosen to use `f32` throughout instead of `f64`.

The image writing is updated a little so that the entire rendered image is
stored in a buffer (`Vec<Color>`) before writing. This change is mainly to make
parallelization easier. Then when writing to the output file it is buffered to
speed things up.

I've switched from writing the image in the ASCII [PPM
format](https://en.wikipedia.org/wiki/Netpbm#Description) to the binary version
(i.e., a magic number of `P6` instead of `P3`).

The dependencies are:

- [`rand`](https://crates.io/crates/rand)
- [`rayon`](https://crates.io/crates/rayon)

## Running

```bash
cargo run --release -- image.ppm
```

## Progress

I finished the book and am working on some performance gains.

### Future work

- Move on to [the next week](https://raytracing.github.io/books/RayTracingTheNextWeek.html)

## License

MIT