//! Camera

use crate::{
    ray::Ray,
    vec3::{Point3, Vec3},
};

pub struct Camera {
    pub origin: Point3,
    pub lower_left_corner: Point3,
    pub horizontal: Vec3,
    pub vertical: Vec3,
    pub u: Vec3,
    pub v: Vec3,
    pub w: Vec3,
    pub lens_radius: f32,
}

impl Camera {
    /// `vfov` is the vertical field-of-view in degrees
    pub fn new(
        look_from: &Point3,
        look_at: &Point3,
        view_up: &Vec3,
        v_fov: f32,
        aspect_ratio: f32,
        aperture: f32,
        focus_dist: f32,
    ) -> Self {
        let theta = v_fov.to_radians();
        let h = f32::tan(theta / 2.);
        let viewport_height = 2. * h;
        let viewport_width = viewport_height * aspect_ratio;

        let w = (look_from - look_at).unit_vector();
        let u = view_up.cross(&w).unit_vector();
        let v = w.cross(&u);

        let origin = *look_from;
        let horizontal = focus_dist * viewport_width * u;
        let vertical = focus_dist * viewport_height * v;
        let lower_left_corner = origin - horizontal / 2. - vertical / 2. - focus_dist * w;

        Camera {
            origin,
            lower_left_corner,
            horizontal,
            vertical,
            w,
            u,
            v,
            lens_radius: aperture / 2.,
        }
    }

    pub fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd = self.lens_radius * Vec3::new_random_in_unit_disk();
        let offset = &self.u * rd.x() + &self.v * rd.y();

        Ray::new(
            self.origin + offset,
            self.lower_left_corner - self.origin + s * self.horizontal + t * self.vertical - offset,
        )
    }
}
