//! The rendered image

use crate::color::Color;
use std::io;

pub struct Image {
    width: usize,
    height: usize,
    data: Vec<Color>,
}

impl Image {
    pub fn new(width: usize, height: usize) -> Self {
        Image {
            width,
            height,
            data: Vec::with_capacity(width * height),
        }
    }

    pub fn push_pixel(&mut self, pixel: &Color) {
        self.data.push(*pixel);
    }

    /// Fill the entire image with the selected `Color`.
    pub fn fill(&mut self, pixel: &Color) {
        self.data.clear();
        self.data.resize(self.width * self.height, *pixel);
    }

    /// Write the image to a buffer in PPM format
    ///
    /// The [Portable PixMap
    /// Map](https://en.wikipedia.org/wiki/Netpbm#Description) variety is the
    /// binary format.
    pub fn write_ppm(&self, w: &mut dyn io::Write, samples_per_pixel: usize) -> io::Result<()> {
        assert_eq!(self.data.len(), self.width * self.height);

        // PPM header
        /// "Portable PixMap", binary type
        const PPM_MAGIC: &str = "P6";
        const MAX_COLORS: usize = 255;
        writeln!(w, "{}", PPM_MAGIC)?;
        writeln!(w, "{} {}", self.width, self.height)?;
        writeln!(w, "{}", MAX_COLORS)?;

        // PPM body
        for pixel in &self.data {
            write_color(w, pixel, samples_per_pixel)?;
        }

        Ok(())
    }
}

/// Write a pixel color in PPM format.
///
/// Three bytes are written which consists of the RGB values ranging from 0 to
/// 255.
fn write_color(w: &mut dyn io::Write, c: &Color, samples: usize) -> io::Result<()> {
    // Divide the color by the number of samples and gamma-correct for gamma=2.0
    let scale = (samples as f32).recip();
    let r = (c.x() * scale).sqrt();
    let g = (c.y() * scale).sqrt();
    let b = (c.z() * scale).sqrt();

    let rgb_tuple = [
        (256. * clamp(r, 0., 0.999)) as u8,
        (256. * clamp(g, 0., 0.999)) as u8,
        (256. * clamp(b, 0., 0.999)) as u8,
    ];

    w.write_all(&rgb_tuple)
}

/// Clamp the value between the minimum and maximum.
///
/// TODO: Replace with
/// [`f32::clamp`](https://doc.rust-lang.org/stable/std/primitive.f32.html#method.clamp)
/// once it stabilizes in Rust 1.50.
fn clamp(x: f32, min: f32, max: f32) -> f32 {
    assert!(min <= max);
    if x < min {
        min
    } else if x > max {
        max
    } else {
        x
    }
}

impl std::ops::AddAssign for Image {
    fn add_assign(&mut self, rhs: Image) {
        assert_eq!(self.width, rhs.width);
        assert_eq!(self.height, rhs.height);
        assert_eq!(self.data.len(), rhs.data.len());

        for (x, y) in self.data.iter_mut().zip(rhs.data.into_iter()) {
            *x += y;
        }
    }
}

impl std::ops::Add for Image {
    type Output = Image;

    fn add(self, rhs: Self) -> Self::Output {
        assert_eq!(self.width, rhs.width);
        assert_eq!(self.height, rhs.height);
        assert_eq!(self.data.len(), rhs.data.len());

        let mut out = Image::new(self.width, self.height);
        for (x, y) in self.data.iter().zip(rhs.data.into_iter()) {
            out.data.push(*x + y);
        }
        out
    }
}
