//! Ray Tracing in One Weekend (in Rust)
//!
//! # Usage
//!
//! `cargo run --release -- image.ppm`
//!

use rand::prelude::*;
use rayon::prelude::*;
use std::{
    env,
    fs::File,
    io::BufWriter,
    path,
    sync::{atomic, Arc},
    time::Instant,
};

use camera::Camera;
use color::Color;
use hittable::{Hittable, HittableList};
use image::Image;
use ray::Ray;
use vec3::{Point3, Vec3};

pub(crate) mod camera;
pub(crate) mod color;
pub(crate) mod hittable;
pub(crate) mod image;
pub(crate) mod material;
pub(crate) mod ray;
pub(crate) mod sphere;
pub(crate) mod vec3;

/// Program options
#[derive(Debug)]
pub(crate) struct ProgOptions {
    /// Output filename
    output: path::PathBuf,
}

/// Parse the command arguments and exit if there's an error
fn parse_args() -> ProgOptions {
    let mut args = env::args();

    if args.len() != 2 {
        let arg0 = env::current_exe()
            .map(|p| p.file_name().unwrap().to_owned())
            .map(|s| s.into_string().unwrap())
            .unwrap_or(format!(
                "{}{}",
                env!("CARGO_PKG_NAME"),
                env::consts::EXE_SUFFIX
            ));
        eprintln!("Weekend ray-tracer");
        eprintln!("Usage: {} <FILE>", arg0);
        eprintln!("The output file is written in PPM format");
        std::process::exit(1);
    }

    let output = args.nth(1).map(path::PathBuf::from).unwrap();

    ProgOptions { output }
}

/// Return a color based on the ray and the world.
fn ray_color(r: &Ray, world: &dyn Hittable, depth: usize) -> Color {
    // If we've exceeded the ray bounce limit, no more light is gathered
    if depth == 0 {
        return Color::new(0., 0., 0.);
    }

    match world.hit(r, 1e-3, f32::INFINITY) {
        Some(hit) => hit
            .mat
            .scatter(r, &hit)
            .map(|(attenuation, scattered)| attenuation * ray_color(&scattered, world, depth - 1))
            .unwrap_or_else(|| Color::new(0., 0., 0.)),
        None => {
            const WHITE: Color = Color::new(1., 1., 1.);
            const LIGHT_BLUE: Color = Color::new(0.5, 0.7, 1.);

            let unit_direction = r.direction().unit_vector();
            let t = (unit_direction.y() + 1.) * 0.5;

            WHITE.lerp(&LIGHT_BLUE, t).into()
        }
    }
}

fn main() {
    let prog_options = parse_args();

    // Image
    const ASPECT_RATIO: f32 = 3. / 2.;
    const IMAGE_WIDTH: usize = 1200;
    const IMAGE_HEIGHT: usize = (IMAGE_WIDTH as f32 / ASPECT_RATIO) as usize;
    const SAMPLES_PER_PIXEL: usize = 500;
    const MAX_DEPTH: usize = 50;

    // World
    let world = HittableList::random_scene();

    // Camera
    let look_from = Point3::new(13., 2., 3.);
    let look_at = Point3::new(0., 0., 0.);
    let view_up = Vec3::new(0., 1., 0.);
    let dist_to_focus = 10.;
    let aperture = 0.1;
    let cam = Camera::new(
        &look_from,
        &look_at,
        &view_up,
        20.,
        ASPECT_RATIO,
        aperture,
        dist_to_focus,
    );

    // Render
    let render_start = Instant::now();
    let counter = Arc::new(atomic::AtomicUsize::new(0));
    eprintln!("Rendering threads: {}", rayon::current_num_threads());
    let image = (0..SAMPLES_PER_PIXEL)
        .into_par_iter()
        .map(|_sample| {
            let prev_count = counter.fetch_add(1, atomic::Ordering::SeqCst);
            eprintln!(
                "Processing sample: {}/{}",
                prev_count + 1,
                SAMPLES_PER_PIXEL
            );

            let mut local_image = Image::new(IMAGE_WIDTH, IMAGE_HEIGHT);
            let mut rng = rand::thread_rng();
            for j in (0..IMAGE_HEIGHT).rev() {
                for i in 0..IMAGE_WIDTH {
                    let u = (i as f32 + rng.gen::<f32>()) / (IMAGE_WIDTH - 1) as f32;
                    let v = (j as f32 + rng.gen::<f32>()) / (IMAGE_HEIGHT - 1) as f32;
                    let r = cam.get_ray(u, v);
                    let pixel_color = ray_color(&r, &world, MAX_DEPTH);
                    local_image.push_pixel(&pixel_color);
                }
            }

            local_image
        })
        .reduce(
            || {
                let mut image_base = Image::new(IMAGE_WIDTH, IMAGE_HEIGHT);
                image_base.fill(&Color::new(0., 0., 0.));
                image_base
            },
            |x, y| x + y,
        );

    eprintln!(
        "Finished rendering {} samples in {:0.2} s",
        counter.load(atomic::Ordering::SeqCst),
        render_start.elapsed().as_secs_f32()
    );

    // Write image to output file
    eprintln!("Writing: {}", prog_options.output.display());
    let write_start = Instant::now();
    {
        let f = File::create(&prog_options.output).expect("Cannot create file");
        let mut buf = BufWriter::new(f);
        image.write_ppm(&mut buf, SAMPLES_PER_PIXEL).unwrap();
    }
    eprintln!(
        "Finished writing in {:0.2} s",
        write_start.elapsed().as_secs_f32()
    );
}
