use crate::vec3::Vec3;

/// An RGB color
///
/// This is a newtype wrapper over `Vec3`. The three elements are the red,
/// green, and blue values for the color.
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Color(Vec3);

impl Color {
    pub const fn new(x: f32, y: f32, z: f32) -> Self {
        Color(Vec3::new(x, y, z))
    }
}

impl std::ops::Deref for Color {
    type Target = Vec3;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::convert::From<Vec3> for Color {
    fn from(v: Vec3) -> Self {
        Color(v)
    }
}

impl std::convert::From<(f32, f32, f32)> for Color {
    fn from(v: (f32, f32, f32)) -> Self {
        Color(v.into())
    }
}

impl std::ops::Mul<f32> for Color {
    type Output = Color;

    fn mul(self, rhs: f32) -> Self::Output {
        Color(rhs * self.0)
    }
}

impl std::ops::Mul for Color {
    type Output = Color;

    fn mul(self, rhs: Self) -> Self::Output {
        Color(self.0 * rhs.0)
    }
}

impl std::ops::AddAssign for Color {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
    }
}

impl std::ops::Add for Color {
    type Output = Color;

    fn add(self, rhs: Self) -> Self::Output {
        Color(self.0 + rhs.0)
    }
}

impl Default for Color {
    fn default() -> Self {
        Color(Vec3::new(0., 0., 0.))
    }
}

impl std::iter::Sum for Color {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(Color::default(), |c, acc| c + acc)
    }
}
