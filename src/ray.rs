//! `Ray` structure

use crate::vec3::{Point3, Vec3};

/// A ray consists of an origin and a direction
pub struct Ray {
    /// Ray origin as a 3d point
    orig: Point3,
    /// Ray direction
    dir: Vec3,
}

impl Ray {
    pub const fn new(origin: Point3, direction: Vec3) -> Self {
        Ray {
            orig: origin,
            dir: direction,
        }
    }

    pub const fn origin(&self) -> Point3 {
        self.orig
    }

    pub const fn direction(&self) -> Vec3 {
        self.dir
    }

    pub fn at(&self, t: f32) -> Point3 {
        self.orig + t * self.dir
    }
}
