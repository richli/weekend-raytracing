//! `Vec3` structure and related types

use rand::prelude::*;

/// Three-element vector
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Vec3 {
    e: [f32; 3],
}

/// A three-dimensional point
///
/// This is a newtype wrapper over `Vec3`. The additional semantics is that
/// arithmetic with a `Point3` and `Vec3` yields a `Point3`, but arithmetic with
/// two `Point3` values yields a `Vec3`.
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Point3(Vec3);

impl Point3 {
    pub const fn new(x: f32, y: f32, z: f32) -> Self {
        Point3(Vec3::new(x, y, z))
    }
}

impl std::ops::Deref for Point3 {
    type Target = Vec3;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::convert::From<Point3> for Vec3 {
    fn from(v: Point3) -> Self {
        v.0
    }
}

impl std::convert::From<(f32, f32, f32)> for Vec3 {
    fn from(v: (f32, f32, f32)) -> Self {
        Vec3::new(v.0, v.1, v.2)
    }
}

impl std::convert::From<(f32, f32, f32)> for Point3 {
    fn from(v: (f32, f32, f32)) -> Self {
        Point3::new(v.0, v.1, v.2)
    }
}

impl std::ops::Add<Vec3> for Point3 {
    type Output = Point3;

    fn add(self, rhs: Vec3) -> Self::Output {
        Point3(self.0 + rhs)
    }
}

impl std::ops::Sub<Vec3> for Point3 {
    type Output = Point3;

    fn sub(self, rhs: Vec3) -> Self::Output {
        Point3(self.0 - rhs)
    }
}

impl std::ops::Sub for Point3 {
    type Output = Vec3;

    fn sub(self, rhs: Self) -> Self::Output {
        self.0 - rhs.0
    }
}

impl std::ops::Sub<&Point3> for Point3 {
    type Output = Vec3;

    fn sub(self, rhs: &Point3) -> Self::Output {
        self.0 - rhs.0
    }
}

impl std::ops::Sub<&Point3> for &Point3 {
    type Output = Vec3;

    fn sub(self, rhs: &Point3) -> Self::Output {
        self.0 - rhs.0
    }
}

impl Vec3 {
    /// Create a new `vec3` with the specified elements
    pub const fn new(e0: f32, e1: f32, e2: f32) -> Self {
        Vec3 { e: [e0, e1, e2] }
    }

    /// Return the `x` or first element
    pub const fn x(&self) -> f32 {
        self.e[0]
    }

    /// Return the `y` or second element
    pub const fn y(&self) -> f32 {
        self.e[1]
    }

    /// Return the `z` or third element
    pub const fn z(&self) -> f32 {
        self.e[2]
    }

    /// Return the square of the l2 norm, or squared Euclidean norm of the vector
    pub fn length_squared(&self) -> f32 {
        self.x().powi(2) + self.y().powi(2) + self.z().powi(2)
    }

    /// Return the l2 norm, or Euclidean norm of the vector
    pub fn length(&self) -> f32 {
        self.length_squared().sqrt()
    }

    /// Return the dot product
    pub fn dot(&self, rhs: &Self) -> f32 {
        self.x() * rhs.x() + self.y() * rhs.y() + self.z() * rhs.z()
    }

    /// Return the cross product
    pub fn cross(&self, rhs: &Self) -> Vec3 {
        Vec3::new(
            self.e[1] * rhs.e[2] - self.e[2] * rhs.e[1],
            self.e[2] * rhs.e[0] - self.e[0] * rhs.e[2],
            self.e[0] * rhs.e[1] - self.e[1] * rhs.e[0],
        )
    }

    /// Normalize by the vector norm
    pub fn unit_vector(&self) -> Self {
        self / self.length()
    }

    /// Linearly interpolate between two `Vec3`s.
    ///
    /// The parameter `t` varies between 0 and 1.
    pub fn lerp(&self, other: &Self, t: f32) -> Self {
        (1.0 - t) * self + t * other
    }

    /// Return a new `Vec3` with each of the three elements randomly chosen from
    /// a uniform distribution between `min` and `max`, exclusive.
    pub fn new_random_within(min: f32, max: f32) -> Self {
        let mut rng = rand::thread_rng();
        Vec3::new(
            rng.gen_range(min..max),
            rng.gen_range(min..max),
            rng.gen_range(min..max),
        )
    }

    /// Return a new `Vec3` that lies within a unit sphere: each of the elements
    /// varies between ±1 and the norm of the vector is < 1.
    pub fn new_random_within_sphere() -> Self {
        // This is implemented using a simple rejection sampling approach
        loop {
            let candidate = Self::new_random_within(-1., 1.);
            if candidate.length_squared() < 1. {
                return candidate;
            }
        }
    }

    /// Return a new `Vec3` the lies on the surface of a unit sphere. Each of
    /// the elements varies between ±1 and the vector is unit norm.
    pub fn new_random_on_sphere() -> Self {
        // This is implmented by picking a random point within a unit sphere and
        // then normalizing to project it to the surface
        Self::new_random_within_sphere().unit_vector()
    }

    /// Return a new `Vec3` from a random point within the unit disk: the `x`
    /// and `y` components are within ±1, `z` is 0, and the norm of the vector
    /// is < 1.
    pub fn new_random_in_unit_disk() -> Self {
        // This is implemented using a simple rejection sampling approach
        loop {
            let mut rng = rand::thread_rng();
            let candidate = Vec3::new(rng.gen_range(-1.0..1.), rng.gen_range(-1.0..1.), 0.);
            if candidate.length_squared() < 1. {
                return candidate;
            }
        }
    }

    /// Return if the vector is close to zero in all directions
    pub fn near_zero(&self) -> bool {
        const EPS: f32 = 1e-6;
        self.e.iter().all(|d| d.abs() < EPS)
    }

    /// Reflect a ray off a normal
    pub fn reflect(&self, normal: &Vec3) -> Self {
        self - &(2. * self.dot(normal) * normal)
    }

    /// Refract a ray through a dielectric
    ///
    /// The `eta_ratio` is the ratio of the refractive indices of incident ray over transmitted ray.
    pub fn refract(&self, normal: &Vec3, eta_ratio: f32) -> Self {
        let cos_theta = -self.dot(normal).min(1.);
        let ray_perp = eta_ratio * (self + &(cos_theta * normal));
        let ray_parallel = -(1. - ray_perp.length_squared()).abs().sqrt() * normal;

        ray_perp + ray_parallel
    }
}

impl Default for Vec3 {
    fn default() -> Self {
        Vec3 { e: [0., 0., 0.] }
    }
}

impl std::ops::Neg for Vec3 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Vec3 {
            e: [-self.e[0], -self.e[1], -self.e[2]],
        }
    }
}

impl std::ops::Index<usize> for Vec3 {
    type Output = f32;

    fn index(&self, index: usize) -> &Self::Output {
        &self.e[index]
    }
}

impl std::ops::IndexMut<usize> for Vec3 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.e[index]
    }
}

impl std::ops::AddAssign for Vec3 {
    fn add_assign(&mut self, rhs: Self) {
        self.e[0] += rhs.e[0];
        self.e[1] += rhs.e[1];
        self.e[2] += rhs.e[2];
    }
}

impl std::ops::MulAssign<f32> for Vec3 {
    fn mul_assign(&mut self, rhs: f32) {
        self.e[0] *= rhs;
        self.e[1] *= rhs;
        self.e[2] *= rhs;
    }
}

impl std::ops::DivAssign<f32> for Vec3 {
    fn div_assign(&mut self, rhs: f32) {
        self.e[0] /= rhs;
        self.e[1] /= rhs;
        self.e[2] /= rhs;
    }
}

impl std::fmt::Display for Vec3 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {}", self.x(), self.y(), self.z())
    }
}

impl std::ops::Add for Vec3 {
    type Output = Vec3;

    fn add(self, rhs: Self) -> Self::Output {
        Vec3::new(
            self.e[0] + rhs.e[0],
            self.e[1] + rhs.e[1],
            self.e[2] + rhs.e[2],
        )
    }
}

impl std::ops::Add<&Vec3> for &Vec3 {
    type Output = Vec3;

    fn add(self, rhs: &Vec3) -> Self::Output {
        Vec3::new(
            self.e[0] + rhs.e[0],
            self.e[1] + rhs.e[1],
            self.e[2] + rhs.e[2],
        )
    }
}

impl std::ops::Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec3::new(
            self.e[0] - rhs.e[0],
            self.e[1] - rhs.e[1],
            self.e[2] - rhs.e[2],
        )
    }
}

impl std::ops::Sub<&Vec3> for &Vec3 {
    type Output = Vec3;

    fn sub(self, rhs: &Vec3) -> Self::Output {
        Vec3::new(
            self.e[0] - rhs.e[0],
            self.e[1] - rhs.e[1],
            self.e[2] - rhs.e[2],
        )
    }
}

impl std::ops::Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, rhs: Self) -> Self::Output {
        Vec3::new(
            self.e[0] * rhs.e[0],
            self.e[1] * rhs.e[1],
            self.e[2] * rhs.e[2],
        )
    }
}

impl std::ops::Mul<&Vec3> for &Vec3 {
    type Output = Vec3;

    fn mul(self, rhs: &Vec3) -> Self::Output {
        Vec3::new(
            self.e[0] * rhs.e[0],
            self.e[1] * rhs.e[1],
            self.e[2] * rhs.e[2],
        )
    }
}

impl std::ops::Mul<f32> for &Vec3 {
    type Output = Vec3;

    fn mul(self, rhs: f32) -> Self::Output {
        Vec3::new(self.e[0] * rhs, self.e[1] * rhs, self.e[2] * rhs)
    }
}

impl std::ops::Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Self::Output {
        Vec3::new(self * rhs.e[0], self * rhs.e[1], self * rhs.e[2])
    }
}

impl std::ops::Mul<&Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, rhs: &Vec3) -> Self::Output {
        Vec3::new(self * rhs.e[0], self * rhs.e[1], self * rhs.e[2])
    }
}

impl std::ops::Div<f32> for Vec3 {
    type Output = Vec3;

    fn div(self, rhs: f32) -> Self::Output {
        let scale = rhs.recip();
        Vec3::new(self.e[0] * scale, self.e[1] * scale, self.e[2] * scale)
    }
}

impl std::ops::Div<f32> for &Vec3 {
    type Output = Vec3;

    fn div(self, rhs: f32) -> Self::Output {
        let scale = rhs.recip();
        Vec3::new(self.e[0] * scale, self.e[1] * scale, self.e[2] * scale)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn build_and_extract() {
        const V: Vec3 = Vec3::new(1., 2., 3.);
        assert_eq!(V.x(), 1.);
        assert_eq!(V.y(), 2.);
        assert_eq!(V.z(), 3.);
    }

    #[test]
    fn check_norms() {
        const V: Vec3 = Vec3::new(3., 4., 5.);
        assert_eq!(V.length_squared(), 50.);
        assert_eq!(V.length(), 5. * std::f32::consts::SQRT_2);
    }

    #[test]
    fn check_neg() {
        const V_ORIG: Vec3 = Vec3::new(-1., 2., -3.);
        const V_NEG: Vec3 = Vec3::new(1., -2., 3.);
        assert_eq!(-V_ORIG, V_NEG);
    }

    #[test]
    fn check_indexing_in_range() {
        let mut v: Vec3 = Vec3::new(1., 2., 3.);

        assert_eq!(v[0], 1.);
        assert_eq!(v[1], 2.);
        assert_eq!(v[2], 3.);

        let y = &mut v[1];
        *y = -10.;
        assert_eq!(v[1], -10.);
    }

    #[test]
    #[should_panic(expected = "index out of bounds")]
    fn check_indexing_out_of_range() {
        let v = Vec3::new(1., 2., 3.);
        let _ = v[3];
    }

    #[test]
    fn check_inplace_addition() {
        let mut v0 = Vec3::default();
        let v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(2., 4., 6.);

        v0 += v1;
        assert_eq!(v0, v1);

        v0 += v1;
        assert_eq!(v0, v2);
    }

    #[test]
    fn check_inplace_scaling() {
        let mut v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(2., 4., 6.);

        v1 *= 2.;
        assert_eq!(v1, v2);
    }

    #[test]
    fn check_inplace_inversion() {
        let v1 = Vec3::new(1., 2., 3.);
        let mut v2 = Vec3::new(2., 4., 6.);

        v2 /= 2.;
        assert_eq!(v1, v2);
    }

    #[test]
    fn check_addition() {
        let v0 = Vec3::default();
        let v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(2., 4., 6.);

        // Check addition of values
        let v01 = v0 + v1;
        assert_eq!(v01, v1);

        // Check addition of references
        let v11 = &v1 + &v1;
        assert_eq!(v11, v2);
    }

    #[test]
    fn check_subtraction() {
        let v0 = Vec3::default();
        let v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(2., 4., 6.);

        // Check subtraction of values
        let v10 = v1 - v0;
        assert_eq!(v10, v1);

        // Check subtraction of references
        let v21 = &v2 - &v1;
        assert_eq!(v21, v1);
    }

    #[test]
    fn check_multiplication() {
        let v0 = Vec3::default();
        let v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(1., 4., 9.);

        // Check multiplication of values
        let v10 = v1 * v0;
        assert_eq!(v10, v0);

        // Check multiplication of references
        let v11 = &v1 * &v1;
        assert_eq!(v11, v2);
    }

    #[test]
    fn check_scaling() {
        let v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(2., 4., 6.);

        assert_eq!(&v1 * 2., v2);
        assert_eq!(2. * &v1, v2);

        assert_eq!(&v2 / 2., v1);
    }

    #[test]
    fn check_normalization() {
        let v1 = Vec3::new(1., 0., 1.);
        let v2 = Vec3::new(
            std::f32::consts::FRAC_1_SQRT_2,
            0.,
            std::f32::consts::FRAC_1_SQRT_2,
        );

        assert_eq!(v1.unit_vector(), v2);
    }

    #[test]
    fn check_dot() {
        let v1 = Vec3::new(1., 2., 3.);
        let v2 = Vec3::new(3., 2., 1.);

        assert_eq!(v1.dot(&v1), v1.length_squared());
        assert_eq!(v1.dot(&v2), 10.);
    }

    #[test]
    fn check_cross() {
        let v1 = Vec3::new(1., 0., 0.);
        let v2 = Vec3::new(0., 1., 0.);
        let v3 = Vec3::new(0., 0., 1.);

        assert_eq!(v1.cross(&v2), v3);

        let v1 = Vec3::new(3., -3., 1.);
        let v2 = Vec3::new(4., 9., 2.);
        let v3 = Vec3::new(-15., -2., 39.);

        assert_eq!(v1.cross(&v2), v3);
    }
}
