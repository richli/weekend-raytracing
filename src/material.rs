//! Material properties

use crate::{color::Color, hittable::HitRecord, ray::Ray, vec3::Vec3};
use rand::prelude::*;

#[derive(Debug)]
pub(crate) enum Material {
    /// A material that has Lambertian (diffuse) reflections
    Lambertian { albedo: Color },
    /// A metallic material
    Metal { albedo: Color, fuzz: f32 },
    /// A dielectric material
    Dielectric { index_of_refraction: f32 },
}

impl Material {
    /// Scatter a ray off the material
    ///
    /// If the ray is scattered (and not absorbed), the output tuple consists of
    /// the attenuation color and the scatter direction.
    pub(crate) fn scatter(&self, r_in: &Ray, rec: &HitRecord) -> Option<(Color, Ray)> {
        match *self {
            Material::Lambertian { albedo } => {
                let scatter_direction = rec.normal + Vec3::new_random_on_sphere();

                // Catch degenerate scatter direction
                let scatter_direction = if scatter_direction.near_zero() {
                    rec.normal
                } else {
                    scatter_direction
                };

                let scattered = Ray::new(rec.p, scatter_direction);
                let attenuation = albedo;
                Some((attenuation, scattered))
            }
            Material::Metal { albedo, fuzz } => {
                let reflected = r_in.direction().unit_vector().reflect(&rec.normal);
                let scattered =
                    Ray::new(rec.p, reflected + fuzz * Vec3::new_random_within_sphere());
                if scattered.direction().dot(&rec.normal) > 0. {
                    Some((albedo, scattered))
                } else {
                    None
                }
            }
            Material::Dielectric {
                index_of_refraction,
            } => {
                let mut rng = rand::thread_rng();

                let refraction_ratio = if rec.front_face {
                    index_of_refraction.recip()
                } else {
                    index_of_refraction
                };

                let unit_direction = r_in.direction().unit_vector();

                let cos_theta = (-unit_direction.dot(&rec.normal)).min(1.);
                let sin_theta = (1. - cos_theta.powi(2)).sqrt();

                let cannot_refract = refraction_ratio * sin_theta > 1.;
                let direction =
                    if cannot_refract || reflectance(cos_theta, refraction_ratio) > rng.gen() {
                        unit_direction.reflect(&rec.normal)
                    } else {
                        unit_direction.refract(&rec.normal, refraction_ratio)
                    };
                let scattered_ray = Ray::new(rec.p, direction);

                let attenuation = Color::new(1., 1., 1.);
                Some((attenuation, scattered_ray))
            }
        }
    }
}

/// Compute dielectric reflectance using Shlick's approximation
fn reflectance(cosine: f32, ref_index: f32) -> f32 {
    let r0 = ((1. - ref_index) / (1. + ref_index)).powi(2);
    r0 + (1. - r0) * (1. - cosine).powi(5)
}
