//! A hittable surface or object

use rand::prelude::*;

use crate::{
    color::Color,
    material::Material,
    ray::Ray,
    sphere::Sphere,
    vec3::{Point3, Vec3},
};

pub(crate) struct HitRecord<'a> {
    pub p: Point3,
    pub normal: Vec3,
    pub mat: &'a Material,
    pub t: f32,
    pub front_face: bool,
}

impl HitRecord<'_> {
    pub fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
        self.front_face = r.direction().dot(&outward_normal) < 0.;
        self.normal = if self.front_face {
            *outward_normal
        } else {
            -*outward_normal
        };
    }
}

pub(crate) trait Hittable: Send + Sync {
    fn hit(&self, r: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord>;
}

pub(crate) struct HittableList {
    pub objects: Vec<Sphere>,
}

impl HittableList {
    pub fn new() -> Self {
        HittableList {
            objects: Vec::new(),
        }
    }

    pub fn add(&mut self, object: Sphere) {
        self.objects.push(object);
    }

    pub fn random_scene() -> Self {
        let mut world = Self::new();

        let ground_material = Material::Lambertian {
            albedo: (0.5, 0.5, 0.5).into(),
        };
        world.add(Sphere::new((0., -1000., 0.).into(), 1000., ground_material));

        let mut rng = rand::thread_rng();
        for a in -11..11 {
            for b in -11..11 {
                let center = Point3::new(
                    a as f32 + 0.9 * rng.gen::<f32>(),
                    0.2,
                    b as f32 + 0.9 * rng.gen::<f32>(),
                );

                if (center - Point3::new(4., 0.2, 0.)).length() > 0.9 {
                    let choose_mat: f32 = rng.gen();

                    if choose_mat < 0.8 {
                        // diffuse
                        let albedo = Color::from(
                            Vec3::new_random_within(0., 1.) * Vec3::new_random_within(0., 1.),
                        );
                        let sphere_material = Material::Lambertian { albedo };
                        world.add(Sphere::new(center, 0.2, sphere_material));
                    } else if choose_mat < 0.95 {
                        // metal
                        let albedo = Color::from(Vec3::new_random_within(0.5, 1.));
                        let fuzz = rng.gen_range(0.0..0.5);
                        let sphere_material = Material::Metal { albedo, fuzz };
                        world.add(Sphere::new(center, 0.2, sphere_material));
                    } else {
                        // glass
                        let sphere_material = Material::Dielectric {
                            index_of_refraction: 1.5,
                        };
                        world.add(Sphere::new(center, 0.2, sphere_material));
                    }
                }
            }
        }

        let material1 = Material::Dielectric {
            index_of_refraction: 1.5,
        };
        world.add(Sphere::new(Point3::new(0., 1., 0.), 1., material1));

        let material2 = Material::Lambertian {
            albedo: (0.4, 0.2, 0.1).into(),
        };
        world.add(Sphere::new(Point3::new(-4., 1., 0.), 1., material2));

        let material3 = Material::Metal {
            albedo: (0.7, 0.6, 0.5).into(),
            fuzz: 0.,
        };
        world.add(Sphere::new(Point3::new(4., 1., 0.), 1., material3));

        world
    }
}

impl Hittable for HittableList {
    fn hit(&self, r: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        // NOTE: My original implementation used the `.fold()` method while
        // iterating over the objects. However, for some reason it adds too much
        // overhead. It's much faster to do this equivalent loop.

        let mut closest_so_far = t_max;
        let mut closest_hit = None;
        for object in &self.objects {
            if let Some(hit) = object.hit(r, t_min, closest_so_far) {
                closest_so_far = hit.t;
                closest_hit = Some(hit);
            }
        }
        closest_hit
    }
}
