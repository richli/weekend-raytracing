//! A sphere

use crate::{
    hittable::{HitRecord, Hittable},
    material::Material,
    vec3::Point3,
};

pub(crate) struct Sphere {
    center: Point3,
    radius: f32,
    mat: Material,
}

impl Sphere {
    pub(crate) fn new(center: Point3, radius: f32, mat: Material) -> Self {
        Sphere {
            center,
            radius,
            mat,
        }
    }
}

impl Hittable for Sphere {
    fn hit(&self, r: &crate::ray::Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let oc = r.origin() - self.center;
        let a = r.direction().length_squared();
        let half_b = oc.dot(&r.direction());
        let c = oc.length_squared() - self.radius.powi(2);

        let discriminant = half_b.powi(2) - a * c;
        if discriminant < 0. {
            None
        } else {
            let sqrtd = discriminant.sqrt();

            // Find the nearest root that lies in the acceptable range
            let root_near = (-half_b - sqrtd) / a;
            let root = if root_near < t_min || root_near > t_max {
                let root_far = (-half_b + sqrtd) / a;
                if root_far < t_min || root_far > t_max {
                    return None;
                } else {
                    root_far
                }
            } else {
                root_near
            };

            let hit_point = r.at(root);
            let outward_normal = (hit_point - self.center) / self.radius;
            let mut hit_record = HitRecord {
                t: root,
                p: hit_point,
                mat: &self.mat,
                normal: (hit_point - self.center) / self.radius,
                front_face: true,
            };
            hit_record.set_face_normal(r, &outward_normal);
            Some(hit_record)
        }
    }
}
